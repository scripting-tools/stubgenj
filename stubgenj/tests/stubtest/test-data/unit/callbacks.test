[case testBiFunctionCallback]
def valid_for_each_cb(k: str, v: float) -> None:
    pass

def invalid_for_each_cb_1(k: str, v: str) -> None:
    pass

def invalid_for_each_cb_2(k: str) -> None:
    pass

from java.util import HashMap
pydict = {'hello': 1.0, 'world': 42.0}
java_map = HashMap(pydict)

java_map.forEach(valid_for_each_cb)
java_map.forEach(invalid_for_each_cb_1) # E: Argument 1 to "forEach" of "HashMap" has incompatible type "Callable[[str, str], None]"; expected "Union[BiConsumer[str, float], Callable[[str, float], None]]"
java_map.forEach(invalid_for_each_cb_2) # E: Argument 1 to "forEach" of "HashMap" has incompatible type "Callable[[str], None]"; expected "Union[BiConsumer[str, float], Callable[[str, float], None]]"


[case testPredicateCallback]
def valid_predicate(k: str) -> bool:
    pass
def invalid_predicate_1() -> bool:
    pass
def invalid_predicate_2(k: int) -> bool:
    pass
def invalid_predicate_3(k: str) -> int:
    pass

from java.util import ArrayList
pylist = ['test', '1', '2']
java_list = ArrayList(pylist)

java_list.removeIf(valid_predicate)
java_list.removeIf(invalid_predicate_1) # E: Argument 1 to "removeIf" of "ArrayList" has incompatible type "Callable[[], bool]"; expected "Union[Predicate[str], Callable[[str], bool]]"
java_list.removeIf(invalid_predicate_2) # E: Argument 1 to "removeIf" of "ArrayList" has incompatible type "Callable[[int], bool]"; expected "Union[Predicate[str], Callable[[str], bool]]"
java_list.removeIf(invalid_predicate_3) # E: Argument 1 to "removeIf" of "ArrayList" has incompatible type "Callable[[str], int]"; expected "Union[Predicate[str], Callable[[str], bool]]"


[case testStreamChainingAndTypeInference]
from java.util.concurrent.atomic import AtomicInteger
from java.util import ArrayList
from java.util.stream import Collectors


def map_str_to_int(v: str) -> int:
    pass


def int_predicate(v: int) -> bool:
    pass


def str_predicate(v: str) -> bool:
    pass


def map_int_to_atomicint(v: int) -> AtomicInteger:
    pass


def map_atomicint_to_bool(v: AtomicInteger) -> bool:
    pass


pylist = ['test', '1', '2']
java_list = ArrayList(pylist)

str_stream = java_list.stream()
reveal_type(str_stream)  # N: Revealed type is "java.util.stream.Stream[builtins.str]"

int_stream = str_stream.map(map_str_to_int)
reveal_type(int_stream)  # N: Revealed type is "java.util.stream.Stream[builtins.int]"

int_stream.filter(str_predicate) # E: Argument 1 to "filter" of "Stream" has incompatible type "Callable[[str], bool]"; expected "Union[Predicate[int], Callable[[int], bool]]"

int_stream_2 = int_stream.filter(int_predicate)
reveal_type(int_stream_2)  # N: Revealed type is "java.util.stream.Stream[builtins.int]"

int_stream_2.map(map_atomicint_to_bool) # E: Argument 1 to "map" of "Stream" has incompatible type "Callable[[AtomicInteger], bool]"; expected "Union[Function[int, <nothing>], Callable[[int], <nothing>]]"

atomicint_stream = int_stream_2.map(map_int_to_atomicint)
java_set = atomicint_stream.collect(Collectors.toSet())
reveal_type(java_set)  # N: Revealed type is "java.util.Set[java.util.concurrent.atomic.AtomicInteger]"
